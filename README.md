This website is an e-commerce store built in Django, using Mezzanine as base CMS, and Cartridge as E-Commerce plugin.

# Deployment instructions

1. Clone repo
2. Ensure that python 2.7 is installed
3. Install pip
4. Install virtualenv with pip install virtualenv
5. Ensure that apache2 is installed
6. Download mysql
7. Download libmysqlclient-dev for mysql dev tools
8. Create a virtual environment within repo
9. Activate virtual environment
10. Install requirements with pip install -r requirements.txt  
11. Log into mysql to create user and database for use with application. Grant mysql user privliges on database.
12. Modify local_settings.py to include proper database credentials
13. Apply django migrations with python manage.py migrate
14. Load fixture data with python manage.py loaddata fixtures/all.json
15. Attempt to start server with python manage.py runserver host:port
16. Download mod_wsgi source for the apache wsgi mod, see instructions below for location. Wget helps here- and I install to /opt/
17. Follow this guide to install the mod: https://modwsgi.readthedocs.io/en/develop/user-guides/quick-installation-guide.html
18. If using apache2, create wsgi.load and wsgi.conf in mods-available
19. Link both created files to mods-enabled using ln -s
20. Install apache2 dev tools with sudo apt-get install apache2-dev
21. Paste LoadModule wsgi_module /usr/lib/apache2/modules/mod_wsgi.so into wsgi.load, depending on where 'make install' placed the mod.so file
22. Paste Apache configs from here into wsgi.conf, and modify for the needs of your site: https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/modwsgi/
23. Check this repo at reponame/apache-config for example config files
24. Make sure you settings.py has database settings and secret key set. mod-wsgi will not use your local_settings file.
25. Make sure your linux box has the en_US locale and it is set as default: https://www.thomas-krenn.com/en/wiki/Configure_Locales_in_Ubuntu
26. Now we have to collect all our static files into one directory. This is necessary because some of our css, js, etc files are inside lib dirs like cartridge
27. Rename STATIC_ROOT in your settings file to 'staticfiles', or whatever you want your collectstatic dir to be.
	- You will have to copy any files or folders prefixed by a period ('.') manually.
28. Modify the prefix in urls.py for the home page to match the slug of the app based on your apache config.
