DEBUG = True

# Make these unique, and don't share it with anybody.
SECRET_KEY = "b8cc2b8a-166e-46e9-8d16-25c1e9c3006b715246ff-5ef3-45f1-9155-87865f459e755691ded1-a754-4c3b-b90b-bf60cf9f850f"
NEVERCACHE_KEY = "175e9d1e-9f28-470b-91e1-dd2358ca50913ee61b11-3491-4a55-a7b2-ba20102e3fc5858395b4-cad0-4265-b172-dded3f7e4c1e"

DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.mysql",
        # DB name or path to database file if using sqlite3.
        "NAME": "opaque_db",
        # Not used with sqlite3.
        "USER": "root",
        # Not used with sqlite3.
        "PASSWORD": "toor",
        # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "127.0.0.1",
        # Set to empty string for default. Not used with sqlite3.
        "PORT": "3306",
    }
}
